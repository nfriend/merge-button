# Merge Button

A browser extension to allow MRs to be merged by mashing a big button.

## Credits

This extension was bootstrapped using
https://github.com/notlmn/browser-extension-template.
